-- chikun :: 2014
-- Loads all fonts from the /fnt folder


-- Font multiplier
local multi = 1

-- Can't do this recursively due to sizes
fnt = {
    -- Credits font
    credits = love.graphics.newFont("fnt/smoolthan.otf", 64 * multi),
    -- Regular font
    regular = love.graphics.newFont("fnt/smoolthan.otf", 24 * multi),
    -- Menu font
    menu = love.graphics.newFont("fnt/smoolthan.otf", 36 * multi),
    -- Splash screen font
    splash  = love.graphics.newFont("fnt/exo2.otf", 96 * multi),
}

-- chikun :: 2014
-- Shorthand bindings for more optimised code

--[[
    Omitted these libraries due to our lack of use
    * love.font
    * love.image
    * love.math
    * love.sound
    * love.thread
]]--

a = love.audio
e = love.event
f = love.filesystem
g = love.graphics
j = love.joystick
k = love.keyboard
m = love.mouse
p = love.physics
s = love.system
t = love.timer
w = love.window

-- Set default scaling mode to prevent artifacts
if flags.enableScaling then
    g.setDefaultFilter("nearest")
else
    g.setDefaultFilter("linear")
end

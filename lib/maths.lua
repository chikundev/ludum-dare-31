-- chikun :: 2014
-- Extra maths functions


-- Seeds the random number generator
math.randomseed(os.time())


-- Clamps a value between two points
function math.clamp(min, value, max)

    -- Confine value to between two points
    value = math.min(max, value)
    value = math.max(min, value)

    return value

end


-- Checks if two rectangle objects overlap
function math.overlap(obj1, obj2)

    return (obj1.x < obj2.x + obj2.w and
            obj2.x < obj1.x + obj1.w and
            obj1.y < obj2.y + obj2.h and
            obj2.y < obj1.y + obj1.h)

end


-- Checks if a table and an object overlap
function math.overlapTable(tab, obj1)

    local value = false

    for key, obj2 in ipairs(tab) do
        if math.overlap(obj1, obj2) then
            value = true
        end
    end

    return value

end


-- Returns the rounded value of an argument
-- Rounded to 'dec' decimal places, or 0 if omitted
function math.round(val, dec)

    -- NOTE: Negative 'dec' values result
    -- in rounding behind the decimals
    -- eg. math.round(4777, -2) == 4800

    -- Get multiplier for decimal rounding
    dec = 10 ^ (dec or 0)

    -- Return rounded number
    return math.floor(val * dec + 0.5) / dec

end


-- Calculate distance between two points
function math.dist(x1, y1, x2, y2)
    return ((x2 - x1) ^ 2 + (y2 - y1) ^ 2) ^ 0.5
end


-- Returns the sign of a value
function math.sign(val)

    -- Default to returning zero
    local returnValue = 0

    if val < 0 then
        returnValue = -1
    elseif val > 0 then
        returnValue = 1
    end

    return returnValue

end

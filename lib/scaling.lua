-- chikun :: 2014
-- Provides scaling data and functions


-- Scale table
scale = {
    mode = "ratio",
    -- Three modes exist
    -- "whole" will scale in integers, placing window in centre
    -- "full" will scale to fit screen, disregarding aspect ratio
    -- "ratio" will scale to fit screen, but keep aspect ratio
    minW = 960,
    minH = 640
}


-- Default scaling mode for Android is ratio
if s.getOS() == "Android" then
    scale.mode = "ratio"
end


-- Provides interface to various scale modes
function scale.perform()

    -- Resets scaling and translation methods
    g.origin()

    -- Screen scaling values
    local w, h = w.getWidth(), w.getHeight()

    -- Perform scaling based on mode, ratio being fallback
    if scale.mode == "whole" then
        scale.performWhole(w, h, scale.minW, scale.minH)
    elseif scale.mode == "full" then
        scale.performFull(w, h, scale.minW, scale.minH)
    else
        scale.performRatio(w, h, scale.minW, scale.minH)
    end

end


-- Scale based on whole numbers
function scale.performWhole(w, h, minW, minH)

    -- Calculate scaling values
    local scaleW, scaleH = math.floor(w / minW), math.floor(h / minH)
    local scaleValue = math.min(scaleW, scaleH)

    -- Calculate trans values
    local transX, transY = (w - minW * scaleValue) / 2, (h - minH * scaleValue) / 2

    g.translate(transX, transY)

    -- Actually scale
    g.scale(scaleValue)

    -- Actually scissor
    g.setScissor(transX, transY, minW * scaleValue, minH * scaleValue)

end


-- Scale based on ratio
function scale.performRatio(w, h, minW, minH)

    -- Calculate scaling values
    local scaleW, scaleH = w / minW, h / minH
    local scaleValue = math.min(scaleW, scaleH)

    -- Calculate trans values
    local transX, transY = (w - minW * scaleValue) / 2, (h - minH * scaleValue) / 2

    g.translate(transX, transY)

    -- Actually scale
    g.scale(scaleValue)

    -- Actually scissor
    g.setScissor(transX, transY, minW * scaleValue, minH * scaleValue)

end


-- Scale fully
function scale.performFull(w, h, minW, minH)

    -- Actually scale
    g.scale(w / minW, h / minH)

end

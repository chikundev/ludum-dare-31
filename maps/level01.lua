return {
  version = "1.1",
  luaversion = "5.1",
  orientation = "orthogonal",
  width = 30,
  height = 20,
  tilewidth = 32,
  tileheight = 32,
  properties = {},
  tilesets = {
    {
      name = "tilesheet",
      firstgid = 1,
      tilewidth = 32,
      tileheight = 32,
      spacing = 0,
      margin = 0,
      image = "../gfx/floor/tilesheet.png",
      imagewidth = 128,
      imageheight = 224,
      properties = {},
      tiles = {}
    },
    {
      name = "tilesheet",
      firstgid = 29,
      tilewidth = 32,
      tileheight = 32,
      spacing = 0,
      margin = 0,
      image = "../gfx/wall/tilesheet.png",
      imagewidth = 64,
      imageheight = 32,
      properties = {},
      tiles = {}
    },
    {
      name = "lavaTileset",
      firstgid = 31,
      tilewidth = 32,
      tileheight = 32,
      spacing = 0,
      margin = 0,
      image = "../gfx/other/lavaTileset.png",
      imagewidth = 32,
      imageheight = 64,
      properties = {},
      tiles = {}
    },
    {
      name = "spikes",
      firstgid = 33,
      tilewidth = 32,
      tileheight = 32,
      spacing = 0,
      margin = 0,
      image = "../gfx/other/spikes.png",
      imagewidth = 32,
      imageheight = 32,
      properties = {},
      tiles = {}
    },
    {
      name = "water",
      firstgid = 34,
      tilewidth = 32,
      tileheight = 32,
      spacing = 0,
      margin = 0,
      image = "../gfx/other/water.png",
      imagewidth = 32,
      imageheight = 64,
      properties = {},
      tiles = {}
    },
    {
      name = "sealTiles",
      firstgid = 36,
      tilewidth = 32,
      tileheight = 32,
      spacing = 0,
      margin = 0,
      image = "../gfx/other/sealTiles.png",
      imagewidth = 128,
      imageheight = 192,
      properties = {},
      tiles = {}
    },
    {
      name = "orbsTilesheet",
      firstgid = 60,
      tilewidth = 16,
      tileheight = 16,
      spacing = 0,
      margin = 0,
      image = "../gfx/other/orbsTilesheet.png",
      imagewidth = 32,
      imageheight = 32,
      properties = {},
      tiles = {}
    }
  },
  layers = {
    {
      type = "tilelayer",
      name = "bg",
      x = 0,
      y = 0,
      width = 30,
      height = 20,
      visible = true,
      opacity = 1,
      properties = {},
      encoding = "lua",
      data = {
        29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29,
        29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29,
        29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29,
        29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29,
        29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29,
        29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29,
        29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29,
        29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29,
        29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29,
        29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29,
        29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29,
        29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29,
        29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29,
        29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29,
        29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29,
        29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29,
        29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29,
        29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29,
        29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29,
        29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29, 29
      }
    },
    {
      type = "objectgroup",
      name = "important",
      visible = true,
      opacity = 1,
      properties = {},
      objects = {
        {
          name = "playerStart",
          type = "",
          shape = "rectangle",
          x = 96,
          y = 576,
          width = 32,
          height = 32,
          visible = true,
          properties = {}
        },
        {
          name = "",
          type = "",
          shape = "rectangle",
          x = 96,
          y = 640,
          width = 32,
          height = 0,
          visible = true,
          properties = {}
        },
        {
          name = "greenOrb",
          type = "",
          shape = "rectangle",
          x = 160,
          y = 96,
          width = 0,
          height = 0,
          gid = 61,
          visible = true,
          properties = {}
        }
      }
    },
    {
      type = "objectgroup",
      name = "movable",
      visible = false,
      opacity = 1,
      properties = {},
      objects = {
        {
          name = "wall1",
          type = "",
          shape = "rectangle",
          x = 640,
          y = 352,
          width = 0,
          height = 0,
          gid = 2,
          visible = true,
          properties = {}
        },
        {
          name = "wall2",
          type = "",
          shape = "rectangle",
          x = 640,
          y = 384,
          width = 0,
          height = 0,
          gid = 2,
          visible = true,
          properties = {}
        },
        {
          name = "stair1",
          type = "",
          shape = "rectangle",
          x = 736,
          y = 256,
          width = 0,
          height = 0,
          gid = 11,
          visible = true,
          properties = {}
        },
        {
          name = "stair2",
          type = "",
          shape = "rectangle",
          x = 736,
          y = 288,
          width = 0,
          height = 0,
          gid = 2,
          visible = true,
          properties = {}
        },
        {
          name = "stair3",
          type = "",
          shape = "rectangle",
          x = 736,
          y = 320,
          width = 0,
          height = 0,
          gid = 2,
          visible = true,
          properties = {}
        },
        {
          name = "stair5",
          type = "",
          shape = "rectangle",
          x = 768,
          y = 288,
          width = 0,
          height = 0,
          gid = 2,
          visible = true,
          properties = {}
        },
        {
          name = "stair4",
          type = "",
          shape = "rectangle",
          x = 768,
          y = 256,
          width = 0,
          height = 0,
          gid = 11,
          visible = true,
          properties = {}
        },
        {
          name = "stair0",
          type = "",
          shape = "rectangle",
          x = 800,
          y = 256,
          width = 0,
          height = 0,
          gid = 11,
          visible = true,
          properties = {}
        },
        {
          name = "topWall21",
          type = "",
          shape = "rectangle",
          x = 384,
          y = 64,
          width = 0,
          height = 0,
          gid = 2,
          visible = true,
          properties = {}
        },
        {
          name = "topWall17",
          type = "",
          shape = "rectangle",
          x = 320,
          y = 64,
          width = 0,
          height = 0,
          gid = 2,
          visible = true,
          properties = {}
        },
        {
          name = "topWall19",
          type = "",
          shape = "rectangle",
          x = 352,
          y = 64,
          width = 0,
          height = 0,
          gid = 2,
          visible = true,
          properties = {}
        },
        {
          name = "topWall15",
          type = "",
          shape = "rectangle",
          x = 288,
          y = 64,
          width = 0,
          height = 0,
          gid = 2,
          visible = true,
          properties = {}
        },
        {
          name = "topWall11",
          type = "",
          shape = "rectangle",
          x = 224,
          y = 64,
          width = 0,
          height = 0,
          gid = 2,
          visible = true,
          properties = {}
        },
        {
          name = "topWall13",
          type = "",
          shape = "rectangle",
          x = 256,
          y = 64,
          width = 0,
          height = 0,
          gid = 2,
          visible = true,
          properties = {}
        },
        {
          name = "topWall1",
          type = "",
          shape = "rectangle",
          x = 64,
          y = 64,
          width = 0,
          height = 0,
          gid = 2,
          visible = true,
          properties = {}
        },
        {
          name = "topWall3",
          type = "",
          shape = "rectangle",
          x = 96,
          y = 64,
          width = 0,
          height = 0,
          gid = 2,
          visible = true,
          properties = {}
        },
        {
          name = "topWall5",
          type = "",
          shape = "rectangle",
          x = 128,
          y = 64,
          width = 0,
          height = 0,
          gid = 2,
          visible = true,
          properties = {}
        },
        {
          name = "topWall7",
          type = "",
          shape = "rectangle",
          x = 160,
          y = 64,
          width = 0,
          height = 0,
          gid = 2,
          visible = true,
          properties = {}
        },
        {
          name = "topWall9",
          type = "",
          shape = "rectangle",
          x = 192,
          y = 64,
          width = 0,
          height = 0,
          gid = 2,
          visible = true,
          properties = {}
        },
        {
          name = "topWall2",
          type = "",
          shape = "rectangle",
          x = 64,
          y = 96,
          width = 0,
          height = 0,
          gid = 2,
          visible = true,
          properties = {}
        },
        {
          name = "topWall4",
          type = "",
          shape = "rectangle",
          x = 96,
          y = 96,
          width = 0,
          height = 0,
          gid = 2,
          visible = true,
          properties = {}
        },
        {
          name = "topWall6",
          type = "",
          shape = "rectangle",
          x = 128,
          y = 96,
          width = 0,
          height = 0,
          gid = 2,
          visible = true,
          properties = {}
        },
        {
          name = "topWall8",
          type = "",
          shape = "rectangle",
          x = 160,
          y = 96,
          width = 0,
          height = 0,
          gid = 2,
          visible = true,
          properties = {}
        },
        {
          name = "topWall10",
          type = "",
          shape = "rectangle",
          x = 192,
          y = 96,
          width = 0,
          height = 0,
          gid = 2,
          visible = true,
          properties = {}
        },
        {
          name = "topWall12",
          type = "",
          shape = "rectangle",
          x = 224,
          y = 96,
          width = 0,
          height = 0,
          gid = 2,
          visible = true,
          properties = {}
        },
        {
          name = "topWall14",
          type = "",
          shape = "rectangle",
          x = 256,
          y = 96,
          width = 0,
          height = 0,
          gid = 2,
          visible = true,
          properties = {}
        },
        {
          name = "topWall16",
          type = "",
          shape = "rectangle",
          x = 288,
          y = 96,
          width = 0,
          height = 0,
          gid = 2,
          visible = true,
          properties = {}
        },
        {
          name = "topWall18",
          type = "",
          shape = "rectangle",
          x = 320,
          y = 96,
          width = 0,
          height = 0,
          gid = 2,
          visible = true,
          properties = {}
        },
        {
          name = "topWall22",
          type = "",
          shape = "rectangle",
          x = 384,
          y = 96,
          width = 0,
          height = 0,
          gid = 2,
          visible = true,
          properties = {}
        },
        {
          name = "topWall20",
          type = "",
          shape = "rectangle",
          x = 352,
          y = 96,
          width = 0,
          height = 0,
          gid = 2,
          visible = true,
          properties = {}
        },
        {
          name = "crushTop",
          type = "",
          shape = "rectangle",
          x = 480,
          y = 448,
          width = 0,
          height = 0,
          gid = 2,
          visible = true,
          properties = {}
        },
        {
          name = "crushBottom",
          type = "",
          shape = "rectangle",
          x = 480,
          y = 544,
          width = 0,
          height = 0,
          gid = 2,
          visible = true,
          properties = {}
        }
      }
    },
    {
      type = "tilelayer",
      name = "foreground",
      x = 0,
      y = 0,
      width = 30,
      height = 20,
      visible = true,
      opacity = 1,
      properties = {},
      encoding = "lua",
      data = {
        2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2,
        2, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2,
        2, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2,
        2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 11, 12, 11, 12, 11, 12, 11, 12, 11, 12, 0, 0, 0, 0, 0, 0, 2,
        2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 0, 0, 0, 0, 0, 0, 2,
        2, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2,
        2, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2,
        2, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 11, 12, 11, 12, 11, 12, 11, 12, 11, 12, 2,
        2, 2, 0, 0, 0, 0, 11, 12, 11, 12, 11, 12, 11, 12, 0, 0, 11, 11, 12, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2,
        2, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2,
        2, 2, 11, 12, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2,
        2, 2, 2, 2, 0, 0, 11, 12, 12, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2,
        2, 2, 2, 2, 0, 0, 2, 2, 2, 11, 12, 12, 11, 12, 33, 33, 11, 12, 11, 12, 2, 11, 12, 11, 12, 0, 0, 0, 0, 2,
        2, 2, 0, 0, 0, 0, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2,
        2, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 11, 12, 11, 2,
        2, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 11, 12, 2, 2, 2, 2,
        2, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 11, 12, 11, 12, 31, 31, 11, 11, 12, 11, 2, 2, 2, 2, 2, 2,
        2, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 2, 2, 2, 32, 32, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2,
        2, 2, 0, 0, 0, 0, 0, 0, 0, 0, 11, 12, 11, 12, 2, 2, 2, 2, 32, 32, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2,
        2, 2, 11, 12, 11, 12, 11, 12, 11, 12, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2
      }
    },
    {
      type = "objectgroup",
      name = "water",
      visible = true,
      opacity = 1,
      properties = {},
      objects = {}
    },
    {
      type = "objectgroup",
      name = "collisions",
      visible = true,
      opacity = 1,
      properties = {},
      objects = {
        {
          name = "",
          type = "",
          shape = "rectangle",
          x = 0,
          y = 0,
          width = 960,
          height = 32,
          visible = true,
          properties = {}
        },
        {
          name = "",
          type = "",
          shape = "rectangle",
          x = 0,
          y = 32,
          width = 64,
          height = 608,
          visible = true,
          properties = {}
        },
        {
          name = "",
          type = "",
          shape = "rectangle",
          x = 64,
          y = 608,
          width = 896,
          height = 32,
          visible = true,
          properties = {}
        },
        {
          name = "",
          type = "",
          shape = "rectangle",
          x = 320,
          y = 576,
          width = 256,
          height = 32,
          visible = true,
          properties = {}
        },
        {
          name = "",
          type = "",
          shape = "rectangle",
          x = 448,
          y = 512,
          width = 128,
          height = 64,
          visible = true,
          properties = {}
        },
        {
          name = "",
          type = "",
          shape = "rectangle",
          x = 928,
          y = 32,
          width = 32,
          height = 576,
          visible = true,
          properties = {}
        },
        {
          name = "",
          type = "",
          shape = "rectangle",
          x = 640,
          y = 512,
          width = 288,
          height = 96,
          visible = true,
          properties = {}
        },
        {
          name = "",
          type = "",
          shape = "rectangle",
          x = 768,
          y = 480,
          width = 160,
          height = 32,
          visible = true,
          properties = {}
        },
        {
          name = "",
          type = "",
          shape = "rectangle",
          x = 832,
          y = 448,
          width = 96,
          height = 32,
          visible = true,
          properties = {}
        },
        {
          name = "",
          type = "",
          shape = "rectangle",
          x = 512,
          y = 384,
          width = 288,
          height = 32,
          visible = true,
          properties = {}
        },
        {
          name = "",
          type = "",
          shape = "rectangle",
          x = 192,
          y = 352,
          width = 96,
          height = 96,
          visible = true,
          properties = {}
        },
        {
          name = "",
          type = "",
          shape = "rectangle",
          x = 288,
          y = 416,
          width = 256,
          height = 32,
          visible = true,
          properties = {}
        },
        {
          name = "",
          type = "",
          shape = "rectangle",
          x = 288,
          y = 384,
          width = 160,
          height = 32,
          visible = true,
          properties = {}
        },
        {
          name = "",
          type = "",
          shape = "rectangle",
          x = 64,
          y = 320,
          width = 64,
          height = 96,
          visible = true,
          properties = {}
        },
        {
          name = "",
          type = "",
          shape = "rectangle",
          x = 192,
          y = 256,
          width = 256,
          height = 32,
          visible = true,
          properties = {}
        },
        {
          name = "",
          type = "",
          shape = "rectangle",
          x = 608,
          y = 224,
          width = 320,
          height = 96,
          visible = true,
          properties = {}
        },
        {
          name = "",
          type = "",
          shape = "rectangle",
          x = 512,
          y = 256,
          width = 96,
          height = 64,
          visible = true,
          properties = {}
        },
        {
          name = "",
          type = "",
          shape = "rectangle",
          x = 64,
          y = 96,
          width = 672,
          height = 64,
          visible = true,
          properties = {}
        }
      }
    },
    {
      type = "objectgroup",
      name = "triggers",
      visible = true,
      opacity = 1,
      properties = {},
      objects = {
        {
          name = "",
          type = "",
          shape = "rectangle",
          x = 736,
          y = 352,
          width = 32,
          height = 32,
          visible = true,
          properties = {
            ["call"] = "moveWall1"
          }
        },
        {
          name = "",
          type = "",
          shape = "rectangle",
          x = 864,
          y = 192,
          width = 32,
          height = 32,
          visible = true,
          properties = {
            ["call"] = "makeStairs"
          }
        },
        {
          name = "",
          type = "",
          shape = "rectangle",
          x = 416,
          y = 64,
          width = 32,
          height = 32,
          visible = true,
          properties = {
            ["call"] = "moveTopWall"
          }
        },
        {
          name = "",
          type = "",
          shape = "rectangle",
          x = 96,
          y = 64,
          width = 32,
          height = 32,
          visible = true,
          properties = {
            ["call"] = "nextLevel"
          }
        },
        {
          name = "",
          type = "",
          shape = "rectangle",
          x = 224,
          y = 448,
          width = 32,
          height = 160,
          visible = true,
          properties = {
            ["call"] = "textHi"
          }
        },
        {
          name = "",
          type = "",
          shape = "rectangle",
          x = 640,
          y = 416,
          width = 32,
          height = 96,
          visible = true,
          properties = {
            ["call"] = "textAketa"
          }
        },
        {
          name = "",
          type = "",
          shape = "rectangle",
          x = 576,
          y = 320,
          width = 32,
          height = 64,
          visible = true,
          properties = {
            ["call"] = "textDontKnow"
          }
        },
        {
          name = "",
          type = "",
          shape = "rectangle",
          x = 256,
          y = 160,
          width = 32,
          height = 96,
          visible = true,
          properties = {
            ["call"] = "textScared"
          }
        }
      }
    },
    {
      type = "objectgroup",
      name = "lava",
      visible = true,
      opacity = 1,
      properties = {},
      objects = {
        {
          name = "",
          type = "",
          shape = "rectangle",
          x = 576,
          y = 512,
          width = 64,
          height = 96,
          visible = true,
          properties = {}
        }
      }
    },
    {
      type = "objectgroup",
      name = "water",
      visible = true,
      opacity = 1,
      properties = {},
      objects = {}
    },
    {
      type = "objectgroup",
      name = "spikes",
      visible = true,
      opacity = 1,
      properties = {},
      objects = {
        {
          name = "",
          type = "",
          shape = "rectangle",
          x = 448,
          y = 384,
          width = 64,
          height = 32,
          visible = true,
          properties = {}
        }
      }
    }
  }
}

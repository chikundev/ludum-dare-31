-- chikun :: 2014
-- Configuration file


function love.conf(game)

    -- Attach a console for Windows debug [DISABLE ON DISTRIBUTION]
    game.console    = true

    -- Version of LÖVE which this game was made for
    game.version    = "0.9.1"

    -- Omit modules due to disuse
    game.modules.math   = false
    game.modules.thread = false

    game.window.resizable = true
    game.window.resizeable = true

    -- Various window settings

    game.window.title   = "Aketa"
    game.window.width   = 960
    game.window.height  = 640

end

-- chikun :: 2014
-- Menu state


-- Temporary state, removed at end of script
local menuState = { }


function resetOrbs()

    -- Player orb collection
    orbCollected = {
        green   = false,
        red     = false,
        blue    = false,
        yellow  = false
    }

    -- Player orb collection
    orbPlaced = {
        green   = false,
        red     = false,
        blue    = false,
        yellow  = false
    }

end


-- On menu create
function menuState:create()

    menuVars = {
        fadeIn = 1,
        nextFade = 0.3
    }


end


-- On menu update
function menuState:update(dt)

    -- Decrease fadeIn
    menuVars.fadeIn = math.max(menuVars.fadeIn - dt, 0)

    if menuVars.fadeIn == 0 and not menuVars.fadeOut then

        menuVars.nextFade = math.max(menuVars.nextFade - dt, 0)

        if menuVars.nextFade == 0 and input.check("action") then

            menuVars.fadeOut = 0

        end

    elseif menuVars.fadeOut then

        menuVars.fadeOut = menuVars.fadeOut + dt

         bgm.mainmenu:setVolume(1 - menuVars.fadeOut)

        if menuVars.fadeOut >= 1 then

            state.change(states.play)

            bgm.mainmenu:stop()

            resetOrbs()

        end

    end

end


-- On menu draw
function menuState:draw()

    -- Set regular colour
    g.setColor(255, 255, 255)

    -- Draw map background
    map.draw(maps.mainmenu)

    -- Set fade colour
    g.setColor(255, 200, 100, 96)

    -- Draw fade box
    g.rectangle('fill', 0, 0, 960, 640)

    -- Draw fade if still need to
    if menuVars.fadeIn > 0 or menuVars.fadeOut then

        -- Set fade colour
        g.setColor(255, 255, 255, (menuVars.fadeOut or menuVars.fadeIn) * 255)

        -- Draw fade box
        g.rectangle('fill', 0, 0, 960, 640)

    else

        -- Set regular colour
        g.setColor(255, 255, 255)

        -- Draw logo
        g.draw(gfx.logo, 480 - gfx.logo:getWidth() / 2, 128)

        -- Set font
        g.setFont(fnt.menu)

        -- Black colour fade in
        g.setColor(0, 0, 0, (1 - (menuVars.nextFade / 0.3)) * 255)

        -- Shadow
        g.printf("Press ACTION to begin.", 0, 401, 961, 'center')

        -- White colour fade in
        g.setColor(255, 255, 255, (1 - (menuVars.nextFade / 0.3)) * 255)

        -- Text
        g.printf("Press ACTION to begin.", 0, 400, 960, 'center')

    end

end


-- On menu kill
function menuState:kill()

    -- Blank out menuVars
    menuVars = nil

    bgm.mainmenu:stop()

end


-- Transfer data to state loading script
return menuState

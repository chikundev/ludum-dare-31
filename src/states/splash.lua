-- chikun :: 2014
-- Splash state


-- Temporary state, removed at end of script
local splashState = { }


-- On state create
function splashState:create()

    g.setBackgroundColor(0, 0, 0)
     -- state.change(states.menu)

    -- Create temporary variables
    splashVars = {
        timer   = 1,
        played  = false,
        stars = { },
        starPos = 0
    }

    for i = 1, 1000 do

        table.insert(splashVars.stars, {
                x = math.random(960),
                y = math.random(1920) - 1920
            })

    end

end


-- On state update
function splashState:update(dt)

    -- Decrease timer
    splashVars.timer = splashVars.timer - dt

    -- If timer is empty
    if splashVars.timer < 0 then

        -- If not playing intro noise
        if not sfx.intro:isPlaying() then

            -- PLAY IT
            sfx.intro:play()

        end

        -- Make stars fall
        splashVars.starPos = splashVars.starPos + dt * 960

        if splashVars.starPos > 1400 and not bgm.mainmenu:isPlaying() then

            bgm.mainmenu:play()

        end

        -- If stars are fully fallen
        if splashVars.starPos >= 1920 then

            -- Change state
            g.setBackgroundColor(0, 0, 0)
            state.change(states.menu)

        -- If stars are half-fallen
        elseif splashVars.starPos > 960 then

            -- Fade using bg colour
            local valahi = (splashVars.starPos - 960) / 960 * 255
            g.setBackgroundColor(valahi, valahi, valahi)

        end

    -- Play the mid-beep
    elseif splashVars.timer <= 0.5 and not splashVars.played then
        sfx.startup:play()
        splashVars.played = true
    end

end


-- On state draw
function splashState:draw()

    if fade then

        g.setBackgroundColor(0, 0, 0)

        fade = nil

    end
    -- Calculate useable alpha value
    local alpha = 1 - math.abs(splashVars.timer - 0.5) * 2

    if splashVars.timer < 0.5 then
        alpha = 1
    end

    -- Draw chikun logo in middle of screen
    g.setFont(fnt.splash)
    g.setColor(255, 255, 255, alpha * 255)
    g.printf("chikun", 0, 272, 960, "center")

    for key, value in ipairs(splashVars.stars) do

        g.rectangle('fill', value.x, value.y + splashVars.starPos, 1, 1)

    end

end


-- On state kill
function splashState:kill()

    -- Kill splashVars
    splashVars = nil

end


-- Transfer data to state loading script
return splashState

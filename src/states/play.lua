-- chikun :: 2014
-- Play state


-- Temporary state, removed at end of script
local playState = { }


-- Important global variables
collisions  = { }
movables    = { }
triggers    = { }

resetOrbs()


-- On state create
function playState:create()

    currentBGM = bgm.firstCorridors

    map.change(maps.level01, true)

    playVars = {
        fade = 0.5
    }

    sfx.wakeup:play()

    player.start = 4

    addText("zzZZzz", 5000)

end


-- On state update
function playState:update(dt)

    playVars.fade = math.max(playVars.fade - dt, 0)

    if playVars.fade == 0 then

        -- Play BGM if not playing
        if not currentBGM:isPlaying() and not sfx.wakeup:isPlaying() and not sfx.artefactGet:isPlaying() and
            not sfx.discGate:isPlaying() and not player.death then

            currentBGM:play()

        end

        for key, trigger in ipairs(triggers) do

            if math.overlap(trigger, player) and not trigger.activated then

                trigger.activated = not map.current.__FUNCTIONS__[trigger.properties.call]()

            end

        end

        updateChomps(dt)

        player:update(dt)

        updateTiles(dt)

        updateTexts(dt)


        if orbPlaced.red and orbPlaced.yellow and
            orbPlaced.blue and orbPlaced.green then

            if not sfx.discGate:isPlaying() then

                currentBGM:stop()

                state.change(states.credits)

            elseif not player.win then

                player.win = 5

                addText("I can finally leave! Thank you!", 3000)

            end

        end

    end

end


-- On state draw
function playState:draw()

    g.setColor(255, 255, 255)

    map.draw()

    player:draw()

    drawTexts()

    if playVars.fade > 0 then

        g.setColor(255, 255, 255, 255 * 2 * playVars.fade)

        g.rectangle('fill', 0, 0, 960, 640)

    end

end


-- On state kill
function playState:kill()

end


-- Transfer data to state loading script
return playState

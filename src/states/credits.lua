-- chikun :: 2014
-- Credits state


-- Temporary state, removed at end of script
local creditsState = { }


-- On credits create
function creditsState:create()

    colour = { }

    colours = {
        { 224, 224, 224 },
        { 160, 160, 160 }
    }

    dir = 0

    fadeIn = 0.5

    setCol = function()

        if colour == colours[1] then
            colour = colours[2]
        else
            colour = colours[1]
        end

        g.setColor(colour)
    end

    timer = 0

    text = "These are the credits"

    sfx.aketaTheme:play()
    --sfx.aketaTheme:seek(200 * (60 / 95))

end


-- On credits update
function creditsState:update(dt)

    fadeIn = math.max(fadeIn - dt, 0)

    if fade then

        fade = math.min(fade + dt, 1)

        if fade == 1 then

            g.setBackgroundColor(0, 0, 0)

            state.change(states.splash)

        end

    end

    if not sfx.aketaTheme:isPlaying() and not fade then

        fade = 0

    end

    dir = dir + (60 / 95) * dt * 120

    if dir >= 360 then

        dir = dir - 360

    end

    local stuff = math.rad(dir)

    local col1, col2 = math.cos(stuff) * 64, math.sin(stuff) * 64

    g.setBackgroundColor(128 + col1, 96 - col1, 128 - col2)

    timer = timer + dt

    if timer > (60 / 95) then

        timer = timer - (60 / 95)

        setCol()

    end

    local ticker = sfx.aketaTheme:tell() / (60 / 95)

    if ticker > 3 then
        text = "Please bear with us" end
    if ticker > 6 then
        text = "WEEOOOWWW" end
    if ticker > 8 then
        text = "" end
    if ticker > 11.8 then
        text = "YES" end
    if ticker > 12.3 then
        text = "" end
    if ticker > 15 then
        text = "UH HUH" end
    if ticker > 16.3 then
        text = "" end
    if ticker > 19 then
        text = "OH YEAH" end
    if ticker > 20.3 then
        text = "" end
    if ticker > 22.6 then
        text = "LET'S DO THIS" end
    if ticker > 24.8 then
        text = "" end
    if ticker > 27.6 then
        text = "OOH OOH" end
    if ticker > 28.3 then
        text = "" end
    if ticker > 31.2 then
        text = "YES" end
    if ticker > 32.4 then
        text = "" end
    if ticker > 35.2 then
        text = "UH HUH" end
    if ticker > 36.4 then
        text = "" end
    if ticker > 39 then
        text = "*KICK* *KICK* *KICK* *KICK*" end
    if ticker > 39.8 then
        text = "" end
    if ticker > 40.5 then
        text = "AKETA MAZED" end
    if ticker > 42 then
        text = "TRAPPED AND FAZED" end
    if ticker > 44 then
        text = "STUCK IN A CORNER" end
    if ticker > 46 then
        text = "SHE LOST HER WAYS" end
    if ticker > 48.5 then
        text = "NO WAY IN" end
    if ticker > 50 then
        text = "NO WAY OUT" end
    if ticker > 52 then
        text = "SHE TURNED HER" end
    if ticker > 54 then
        text = "STORY INSIDE-OUT" end
    if ticker > 56 then
        text = "~ Creators ~\nKeiran Dawson" end
    if ticker > 58 then
        text = "~ Creators ~\nCohen Dennis" end
    if ticker > 60 then
        text = "~ Creators ~\nMathew Dwyer" end
    if ticker > 62 then
        text = "~ Creators ~\nJosef Frank" end
    if ticker > 64 then
        text = "~ Character Design ~\nKeiran Dawson" end
    if ticker > 66 then
        text = "~ Lead Musician ~\nCohen Dennis" end
    if ticker > 68 then
        text = "~ Lead Artist ~\nMathew Dwyer" end
    if ticker > 70 then
        text = "~ Lead Programmer ~\nJosef Frank" end
    if ticker > 72.8 then
        text = "" end
    if ticker > 75.6 then
        text = "OOH OOH" end
    if ticker > 76.3 then
        text = "" end
    if ticker > 79.2 then
        text = "YES" end
    if ticker > 80.4 then
        text = "" end
    if ticker > 83.2 then
        text = "UH HUH" end
    if ticker > 84.4 then
        text = "" end
    if ticker > 87 then
        text = "*KICK* *KICK* *KICK* *KICK*" end
    if ticker > 88 then
        text = "<3" end
    if ticker > 88.4 then
        text = "THE PATH WAS CONFUSING" end
    if ticker > 90.4 then
        text = "THOUGHTS SHE WAS MUSING" end
    if ticker > 92.4 then
        text = "ABOUT GIVING UP" end
    if ticker > 94.4 then
        text = "FOREVER LOSING" end
    if ticker > 96.4 then
        text = "SHE KEPT STRONG" end
    if ticker > 98.4 then
        text = "PULLED ALONG" end
    if ticker > 100.4 then
        text = "MADE HER WAY OUTSIDE" end
    if ticker > 102.7 then
        text = "HEARS MUSIC" end
    if ticker > 104 then
        text = "" end
    if ticker > 40.5 + 64 then
        text = "AKETA MAZED" end
    if ticker > 42 + 64 then
        text = "TRAPPED AND FAZED" end
    if ticker > 44 + 64 then
        text = "STUCK IN A CORNER" end
    if ticker > 46 + 64 then
        text = "SHE LOST HER WAYS" end
    if ticker > 48.5 + 64 then
        text = "NO WAY IN" end
    if ticker > 50 + 64 then
        text = "NO WAY OUT" end
    if ticker > 52 + 64 then
        text = "SHE TURNED HER" end
    if ticker > 54 + 64 then
        text = "STORY INSIDE-OUT" end
    if ticker > 120 then
        text = "~ Map Design ~\nMathew Dwyer" end
    if ticker > 122 then
        text = "~ Map Design ~\nJosef Frank" end
    if ticker > 124 then
        text = "~ Support Programmer ~\nMathew Dwyer" end
    if ticker > 126 then
        text = "~ Support Musician ~\nJosef Frank" end
    if ticker > 128 then
        text = "~ Special Thanks ~\nChris Alderton" end
    if ticker > 130 then
        text = "~ Special Thanks ~\nMark Moore" end
    if ticker > 132 then
        text = "~ Special Thanks ~\nBradley Roope" end
    if ticker > 134 then
        text = "~ Special Thanks ~\nGage Wilkins" end
    if ticker > 72.8 + 64 then
        text = "" end
    if ticker > 75.6 + 64 then
        text = "OOH OOH" end
    if ticker > 76.3 + 64 then
        text = "" end
    if ticker > 79.2 + 64 then
        text = "YES" end
    if ticker > 80.4 + 64 then
        text = "" end
    if ticker > 83.2 + 64 then
        text = "UH HUH" end
    if ticker > 84.4 + 64 then
        text = "" end
    if ticker > 87 + 64 then
        text = "*KICK* *KICK* *KICK* *KICK*" end
    if ticker > 152 then
        text = "" end
    if ticker > 152.5 then
        text = "COLLECTED ORBS" end
    if ticker > 154 then
        text = "FOUND THE GATE" end
    if ticker > 156 then
        text = "AVOIDED HAZARDS" end
    if ticker > 157.5 then
        text = "SHE WAS" end
    if ticker > 158 then
        text = "TEMPTING FATE" end
    if ticker > 160 then
        text = "THE SHIFTING TILES" end
    if ticker > 162 then
        text = "CARVED A WAY" end
    if ticker > 164 then
        text = "FOR HER" end
    if ticker > 165 then
        text = "TO HAVE" end
    if ticker > 166 then
        text = "A BRIGHTER DAY" end
    if ticker > 88.4 + 80 then
        text = "THE PATH WAS CONFUSING" end
    if ticker > 90.4 + 80 then
        text = "THOUGHTS SHE WAS MUSING" end
    if ticker > 92.4 + 80 then
        text = "ABOUT GIVING UP" end
    if ticker > 94.4 + 80 then
        text = "FOREVER LOSING" end
    if ticker > 96.4 + 80 then
        text = "SHE KEPT STRONG" end
    if ticker > 98.4 + 80 then
        text = "PULLED ALONG" end
    if ticker > 100.4 + 80 then
        text = "MADE HER WAY OUTSIDE" end
    if ticker > 102.7 + 80 then
        text = "HEARS MUSIC" end
    if ticker > 104 + 80 then
        text = "" end
    if ticker > 40.5 + 64 + 80 then
        text = "AKETA MAZED" end
    if ticker > 42 + 64 + 80 then
        text = "TRAPPED AND FAZED" end
    if ticker > 44 + 64 + 80 then
        text = "STUCK IN A CORNER" end
    if ticker > 46 + 64 + 80 then
        text = "SHE LOST HER WAYS" end
    if ticker > 48.5 + 64 + 80 then
        text = "NO WAY IN" end
    if ticker > 50 + 64 + 80 then
        text = "NO WAY OUT" end
    if ticker > 52 + 64 + 80 then
        text = "SHE TURNED HER" end
    if ticker > 54 + 64 + 80 then
        text = "STORY INSIDE-OUT" end
    if ticker > 200 then
        text = "SHE NEVER THOUGHT" end
    if ticker > 201.6 then
        text = "SHE'D MAKE IT OUT" end
    if ticker > 204 then
        text = "BUT HERE WE ARE" end
    if ticker > 205.6 then
        text = "NOW AT THE END" end
    if ticker > 208 then
        text = "SHE SAYS GOODBYE" end
    if ticker > 210.5 then
        text = "AND YOU SHOULD TOO" end
    if ticker > 212.3 then
        text = "YOU'RE TOTALLY" end
    if ticker > 214 then
        text = "THE COOLEST FRIEND" end
    if ticker > 215.5 then
        text = "*CLAP*" end
    if ticker > 216 then
        text = "*SYNTH SOLO*" end
    if ticker > 218 then
        text = "YEH" end
    if ticker > 218.5 then
        text = "YEH-YEH-YEH-YEH-YEH" end
    if ticker > 220 then
        text = "*SYNTH SOLO*" end
    if ticker > 222 then
        text = "AH" end
    if ticker > 222.5 then
        text = "AH-AH-AH-AH-AH" end
    if ticker > 224 then
        text = "*SYNTH SOLO*" end
    if ticker > 226 then
        text = "OH" end
    if ticker > 226.5 then
        text = "OH-OH-OH-OH-OH" end
    if ticker > 228 then
        text = "*SYNTH SOLO*" end
    if ticker > 230 then
        text = "LE" end
    if ticker > 230.5 then
        text = "LE-LE-LE-LE-LE" end
    if ticker > 232 then
        text = "LET'S DO THIS" end
    if ticker > 234 then
        text = "~ Special Thanks ~\nYou" end
    if ticker > 234.5 then
        text = "<3" end

    if fade then

        if fade == 1 then

            g.setBackgroundColor(0, 0, 0)

        end

    end

end


-- On credits draw
function creditsState:draw()

    -- Draw border
    for i = 1, 30 do
        setCol()
        g.rectangle('fill', (i - 1) * 32, 0, 32, 32)
    end

    for i = 1, 19 do
        setCol()
        g.rectangle('fill', 29 * 32, i * 32, 32, 32)
    end

    for i = 29, 1, -1 do
        setCol()
        g.rectangle('fill', (i - 1) * 32, 19 * 32, 32, 32)
    end

    for i = 18, 1, -1 do
        setCol()
        g.rectangle('fill', 0, i * 32, 32, 32)
    end

    -- Draw text
    g.setFont(fnt.credits)

    local x = {text:gsub("\n", "")}

    local height = g.getFont():getHeight() * (x[2] + 1)

    g.setColor(0, 0, 0)
    g.printf(text, 1, 321 - height/2, 960, 'center')
    g.setColor(255, 255, 255)
    g.printf(text, 0, 320 - height/2, 960, 'center')

    -- Draw fade if happening
    g.setColor(0, 0, 0, (fade or 0) * 255)
    g.rectangle('fill', 0, 0, 960, 640)

    -- Draw fade if happening
    g.setColor(255, 255, 255, (fadeIn * 2) * 255)
    if fadeIn > 0 then

        map.draw()

    end

end


-- On credits kill
function creditsState:kill()

end


-- Transfer data to state loading script
return creditsState

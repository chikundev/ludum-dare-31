-- chikun :: 2014
-- Player functions


-- Player table
player = { }


-- On player creation
function player:create(x, y, w, h)

    -- Set player variables
    player.x    = x + (w / 2) - 8
    player.y    = y + 2
    player.w    = 16
    player.h    = h - 2
    player.xStart   = x
    player.yStart   = y
    player.image    = gfx.player.standing

    -- Player speed variables
    player.acceleration = 4000
    player.blinkCycle   = 0
    player.facing       = 1
    player.gravity      = 2400
    player.jumpExtra    = 150
    player.jumpHeight   = 300
    player.maxSpeed     = 180
    player.spawnTimer   = 0.6
    player.terminalVelo = 512
    player.walkCycle    = 0
    player.xSpeed   = 0
    player.ySpeed   = 0

end


function player:update(dt)

    if player.death then
        player.death = player.death - dt * 2

        if player.death <= 0 then

            player.kill()

        end

    elseif player.win then

        player.win = math.max(player.win - dt, 0)

    elseif player.start then

        player.start = math.max(player.start - dt, 0)

        if player.start == 0 then

            player.start = nil

        end

    else

        -- Count down spawn timer
        player.spawnTimer = math.max(player.spawnTimer - dt * 2, 0)

        -- Horizontal player movement

        -- Set temporary movement cariables
        local xMove, yMove = 0, 0

        -- Detect which way we will move
        if input.check('left')  then xMove = -1 end
        if input.check('right') then xMove = xMove + 1 end
        if input.check('up')    then yMove = -1 end
        if input.check('down')  then yMove = yMove + 1 end

        -- Drag player to halt if not wanting to move
        if xMove == 0 and player.xSpeed ~= 0 then

            -- Figure out current direction of player movement
            local hdir = math.sign(player.xSpeed)

            -- Move xSpeed into the opposite direction
            player.xSpeed = player.xSpeed - (dt * hdir * player.maxSpeed * 12)

            -- If slowed down so much that xSpeed is reversed
            if math.sign(player.xSpeed) ~= hdir then

                -- Halt xSpeed
                player.xSpeed = 0

            end

        -- Speed up player if wanting to move
        else

            player.xSpeed = math.clamp(-player.maxSpeed,
                player.xSpeed + xMove * dt * player.acceleration,
                player.maxSpeed)
        end

        -- Actually move player horizontally
        player.x = player.x + (player.xSpeed * dt)

        local crushX, xPrev = false, player.x

        local leftPlayer = {
            x = player.x,
            y = player.y,
            w = 2,
            h = player.h
        }
        local rightPlayer = {
            x = player.x + player.w - 2,
            y = player.y,
            w = 2,
            h = player.h
        }

        -- Check if player is colliding
        while not (math.overlapTable(collisions, leftPlayer) or math.overlapTable(movables, leftPlayer)) ~=
            not (math.overlapTable(collisions, rightPlayer) or math.overlapTable(movables, rightPlayer)) do

            local movement = 1

            if (math.overlapTable(collisions, rightPlayer) or math.overlapTable(movables, rightPlayer)) then

                movement = -1

            end

            -- If so, move player horizontally back
            player.x = math.round(player.x) + movement

            leftPlayer.x = player.x
            rightPlayer.x = player.x + player.w - 2

        end

        if (math.overlapTable(collisions, player) or math.overlapTable(movables, player)) then
            player.x = xPrev
            crushX = true
        end

        -- Change player facing animation
        if player.xSpeed ~= 0 then
            player.facing = math.sign(player.xSpeed)
        end


        -- Vertical player movement

        -- Temporary jump pad
        local jumpPad = {
            x = player.x,
            y = player.y + player.h,
            w = player.w,
            h = 1
        }

        -- Temporary above pad
        local abovePad = {
            x = player.x,
            y = player.y - 1,
            w = player.w,
            h = 1
        }

        -- Jump if physically can or part of extra jump
        if (((math.overlapTable(collisions, jumpPad) or math.overlapTable(movables, jumpPad)) and
                not player.jumpPressed) or
                player.jumpNew) and input.check('action') and
                not (math.overlapTable(collisions, abovePad) or math.overlapTable(movables, abovePad))then

            -- Extra jump mechanics
            if not player.jumpNew then

                player.jumpNew = true
                player.jumpExtra = 256

                sfx.jump:setPitch(0.9 + math.random(10) / 50)

                sfx.jump:play()

            else

                player.jumpExtra = player.jumpExtra - (1024 * dt)
                if player.jumpExtra <= 0 then
                    player.jumpNew = false
                end

            end

            -- Actually jump
            player.ySpeed = -player.jumpHeight

        else

            -- Otherwise forget about extra jump
            player.jumpNew = false

        end

        player.jumpPressed = input.check('action')

        -- Increase player ySpeed
        player.ySpeed = player.ySpeed + (player.gravity * dt)

        if player.ySpeed > player.terminalVelo then

            player.ySpeed = player.terminalVelo

        end

        -- Make player fall
        player.y = player.y + (player.ySpeed * dt)

        -- Discover direction of player's ySpeed
        local sign = math.sign(player.ySpeed)

        if sign == 0 then sign = 1 end

        local crushY, yPrev = false, player.y

        local topPlayer = {
            x = player.x,
            y = player.y,
            w = player.w,
            h = 2
        }
        local lowPlayer = {
            x = player.x,
            y = player.y + player.h - 2,
            w = player.w,
            h = 2
        }

        -- While colliding...
        while not (math.overlapTable(collisions, topPlayer) or math.overlapTable(movables, topPlayer)) ~=
            not (math.overlapTable(collisions, lowPlayer) or math.overlapTable(movables, lowPlayer)) do

            local movement = 1

            if (math.overlapTable(collisions, lowPlayer) or math.overlapTable(movables, lowPlayer)) then

                movement = -1

            end

            -- If so, move player vertically back
            player.y = math.ceil(player.y + movement)

            player.ySpeed = 0

            topPlayer.y = player.y
            lowPlayer.y = player.y + player.h - 2

        end

        if (math.overlapTable(collisions, player) or math.overlapTable(movables, player)) then
            player.y = yPrev
            crushY = true
        end

        if crushX and crushY then

            player.death = 0.8

            sfx.death:play()

            currentBGM:stop()

        end

        -- Update player's image

        -- If standing, make standing, or randomly a blink
        if player.xSpeed == 0 and player.ySpeed == 0 then

            if player.image ~= gfx.player.standing and
               player.image ~= gfx.player.blinking then
                player.blinkCycle = 0
            end

            player.blinkCycle = player.blinkCycle + dt

            if player.blinkCycle > 3 then
                player.blinkCycle = player.blinkCycle - 3
            end

            if player.blinkCycle > 2.5 then
                player.image = gfx.player.blinking
            else
                player.image = gfx.player.standing
            end

        elseif player.ySpeed > 0 then

            player.image = gfx.player.falling

        elseif player.ySpeed < 0 then

            player.image = gfx.player.jumping

        elseif player.xSpeed ~= 0 then

            local playerWalks = {
                gfx.player.standing,
                gfx.player.walking.step1,
                gfx.player.standing,
                gfx.player.walking.step2
            }

            player.walkCycle = player.walkCycle + math.abs(player.xSpeed) * dt / player.maxSpeed * 14

            if player.walkCycle >= #playerWalks then
                player.walkCycle = player.walkCycle - #playerWalks
            end

            player.image = playerWalks[math.floor(player.walkCycle + 1)]

        end


        -- Kill player if need

        if math.overlapTable(hazards, player) and not player.death then

            player.death = 0.8

            sfx.death:play()

            currentBGM:stop()

        end

        for key, orb in ipairs(orbs) do

            if math.overlap(player, orb) then

                orb.obj.x = -256
                orb.obj.y = -256

                orbCollected[orb.col] = true

                if orb.col == "green" then

                    addText("Oh, I think this is an orb.", 3000)

                end

                sfx.artefactGet:play()

                currentBGM:stop()

                table.remove(orbs, key)

            end

        end

        if seal then

            if math.overlap(seal, player) then

                for key, value in pairs(orbPlaced) do

                    if value ~= orbCollected[key] then

                        orbPlaced[key] = true

                        currentBGM:stop()

                        sfx.discGate:play()

                        if key == "green" then

                            addText("Huh. It fits perfectly.", 3000)
                            addText("Maybe I can find more.", 3000)

                        end

                    end

                end

            end

        end

    end

end


function player:draw()

    -- Clear drawing colour
    g.setColor(255, 255, 255, (player.win or 5) / 5 * 255)

    if player.start then

        local var, image = math.floor(player.start) % 2, gfx.player.standing

        if var == 0 then

            image = gfx.player.blinking

        end

        g.draw(image, player.x + 16, player.y + 16, math.rad(270), 1, 1, 16, 16)

    else

        if not player.death then

            -- Draw player
            g.draw(player.image, math.round(player.x + player.image:getWidth() / 2),
                math.round(player.y), 0, player.facing, 1, player.image:getWidth() / 2,
                player.image:getHeight() - player.h)

        end

        if player.spawnTimer > 0 or player.death then

            local image = gfx.player.respawn["flash"  .. math.ceil(player.spawnTimer / 0.2)]

            if player.death then

                image = gfx.player.death["spin"  .. math.ceil(player.death / 0.2)]

            end

            g.draw(image, math.round(player.x + image:getWidth() / 2),
            math.round(player.y), 0, player.facing, 1, image:getWidth() / 2,
            image:getHeight() - player.h)

        end

    end

end


function player.kill()

    currentText = { }

    chompList   = { }

    if map.current == maps.level05 then

        orbCollected.yellow = false

    end

    map.restart()

    player.spawnTimer = 0.6

    player.death = nil

    currentBGM:play()

end

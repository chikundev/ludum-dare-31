currentText = { }
tilesToMove = { }
chompList   = { }

function moveTile(tileName, toX, toY, toDelay)

    -- Delay is in milliseconds btw

    local newTile = { }

    for key, tile in ipairs(movables) do

        if tile.name == tileName then

            newTile = tile

        end

    end

    -- Add tile movement to table
    table.insert(tilesToMove, {
            tile = newTile,
            x = toX * 32,
            y = toY * 32,
            delay = toDelay / 1000,
            count = 0.1,
            origX = newTile.x,
            origY = newTile.y
        })

end


function killAllChomps()

    for key, chomp in ipairs(chompList) do

        chomp.dir = -1

        chomp.delay = 0

        chomp.kill = true

        chomp.speed = 0.1

    end

end




function killCertainChomps(tileName)

    for key, chomp in ipairs(chompList) do

        if chomp.object.name == tileName then

            chomp.dir = -1

            chomp.delay = 0

            chomp.kill = true

            chomp.speed = 0.1

        end

    end

end


function moveAllRelative(tileName, plusX, plusY, toDelay)

    -- Delay is in milliseconds btw

    for key, newTile in ipairs(movables) do

        if newTile.name == tileName then

            -- Add tile movement to table
            table.insert(tilesToMove, {
                    tile = newTile,
                    x = newTile.x,
                    y = newTile.y,
                    delay = toDelay / 1000,
                    count = 0.1,
                    origX = newTile.x,
                    origY = newTile.y,
                    px = plusX,
                    py = plusY,
                    spec = true
                })

        end

    end

end


function addText(text, timer)

    table.insert(currentText, {
            content = text,
            count = ((timer / 1000) or 4),
            fadeIn = 0.5
        })

end


function updateTiles(dt)

    toDelete = { }

    for key, object in ipairs(tilesToMove) do

        if object.delay > 0 then object.delay = object.delay - dt
        else

            if object.count == 0.1 and object.spec then

                object.origX = object.tile.x
                object.origY = object.tile.y

                object.x = object.tile.x + object.px * 32
                object.y = object.tile.y + object.py * 32

            end

            object.count = math.max(object.count - dt, 0)

            object.tile.x = object.x + (object.origX - object.x) * object.count * 10

            object.tile.y = object.y + (object.origY - object.y) * object.count * 10

            if object.count == 0 then
                table.insert(toDelete, key)
            end
        end

    end

    if #toDelete > 0 then

        forcePlay(sfx.slide)

        for i = #toDelete, 1, -1 do



            table.remove(tilesToMove, toDelete[i])

        end

    end

end


function updateChomps(dt)

    chompsToKill = { }

    for key, chomp in ipairs(chompList) do

        chomp.delay = math.max(chomp.delay - dt, 0)

        if chomp.delay == 0 then

            if chomp.dir == 1 then

                chomp.val = math.min(chomp.val + dt / chomp.speed, 1)

                if chomp.val == 1 then

                    chomp.dir = -1
                    chomp.delay = chomp.closeTime

                    forcePlay(sfx.slide)

                end

            else

                chomp.val = math.max(chomp.val - dt / chomp.speed, 0)

                if chomp.val == 0 then

                    chomp.dir = 1
                    chomp.delay = chomp.openTime

                    if chomp.kill then

                        table.insert(chompsToKill, key)

                    end

                end

            end

        end

        chomp.object.x = chomp.object.ox + (chomp.toX - chomp.object.ox) * (chomp.val)
        chomp.object.y = chomp.object.oy + (chomp.toY - chomp.object.oy) * (chomp.val)

    end

    for i=#chompsToKill, 1, -1 do

        table.remove(chompList, chompsToKill[i])

    end

end


function addChomp(tileName, plusX, plusY, speedT, openTimeT, closeTimeT)

    -- Delay is in milliseconds btw

    for key, newTile in ipairs(movables) do

        if newTile.name == tileName then

            -- Add tile movement to table
            table.insert(chompList, {
                    object = newTile,
                    toX = newTile.x + plusX * 32,
                    toY = newTile.y + plusY * 32,
                    delay = 0,
                    dir = 1,
                    val = 0,
                    speed = speedT / 1000,
                    openTime = openTimeT / 1000,
                    closeTime = closeTimeT / 1000
                })

            newTile.ox = newTile.x
            newTile.oy = newTile.y

        end

    end

end


function updateTexts(dt)

    deleteText = false

    if #currentText > 0 then

        local obj = currentText[1]

        if obj.fadeIn == 0.5 then

            sfx.talk:play()

        end

        obj.fadeIn  = math.max(obj.fadeIn - dt, 0)

        obj.count   = math.max(obj.count  - dt, 0)

        if obj.count == 0 then

            table.remove(currentText, 1)

        end

    end

end


function drawTexts()

    if #currentText > 0 then

        local text = currentText[1].content

        local align = 'left'

        if currentText[1].fadeIn > 0 then

            text = text:sub(1, text:len() * (1 - (currentText[1].fadeIn / 0.5)))

        elseif currentText[1].count < 0.5 then

            align = 'right'

            text = text:sub(text:len() * (1 - (currentText[1].count / 0.5)), -1)

        end

        textWidth = fnt.regular:getWidth(currentText[1].content)

        textX = math.clamp(16, player.x + (player.w / 2) - (textWidth / 2), 960 - textWidth - 16)
        textY = math.max(16, player.y - 40)

        g.setFont(fnt.regular)

        g.setColor(0, 0, 0)
        g.printf(text, textX + 1, textY + 1, textWidth, align)
        g.setColor(255, 255, 255)
        g.printf(text, textX, textY, textWidth, align)

    end

end


function forcePlay(sound)

    sound:stop()

    sound:play()

end

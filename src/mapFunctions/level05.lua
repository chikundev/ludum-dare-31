return {

    moveLevel = function()

        chmpz = false

        orbCollected.yellow = false
        orbCollected.blue = false

        currentBGM:stop()

        currentBGM = bgm.world2

        moveAllRelative('d', 0, 2, 0)

        moveAllRelative('l1', -1, 0, 0)
        moveAllRelative('l2', -2, 0, 0)
        moveAllRelative('l3', -3, 0, 0)

    end,
    newPath = function()

        moveTile('le2', 12, 5, 0)
        moveTile('le3', 11, 6, 0)

    end,
    chomps = function()

        addChomp('lchomp', -2, 0, 500, 1000, 1000)
        addChomp('rchomp', 2, 0, 500, 1000, 1000)

        addText("Wow OK", 2000)

    end,
    chompzDed = function()

        killAllChomps()

    end,
    removeOrb = function()

        orbCollected.yellow = false
        orbCollected.blue = false

    end,
    chompzDed2 = function()

        killAllChomps()

        chmpz = true

        if not orbCollected.yellow then

            return true

        end

        addText("Almost there!", 2000)

    end,
    vChomps = function()


        addChomp('l2', 0, -2, 300, 700, 700)
        addChomp('l3', 0, -2, 300, 700, 700)

        moveTile('r1', 18, 10, 0)
        moveTile('r2', 12, 8, 0)
        moveTile('r3', 8, 8, 0)

        currentBGM = bgm.world3

        addText("That's three!", 2000)

    end,
    nextLevel = function()

    end,
    four = function()

        addText("And that's all four!", 2000)

    end

}

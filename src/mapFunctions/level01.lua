return {
    moveWall1 = function()

        -- Move Walls
        moveTile('wall1', 20, 8, 0)
        moveTile('wall2', 20, 9, 0)

    end,

    makeStairs = function()

        --Raise Stairs
        moveTile('stair0', 25, 6, 200)

        for s=1, 3 do
                moveAllRelative('stair' .. s, 0, -3, 200)
        end

        for s=1, 3 do
                moveAllRelative('stair' .. (s + 3), 0, -2, 200)
        end

        killAllChomps()

    end,

    moveTopWall = function()

        --Move Walls At Top
        for i=1, 22 do
                moveAllRelative('topWall' .. i, 0, 2, 0)
        end

        currentBGM:stop()

        currentBGM = bgm.world1

        addText("What's that?", 2000)

    end,

    nextLevel = function()

        -- If player has collected green orb
        if orbCollected.green then

            --Changes to level 2
            map.change(maps.level02, false)

        else

            -- Otherwise, this obscure piece of code allows the trigger to repeat
            return true

        end

    end,

    textHi = function()

        addText("Hello.", 2000)

        addChomp('crushTop', 0, 1, 500, 1000, 1000)

        addChomp('crushBottom', 0, -1, 500, 1000, 1000)

    end,

    textAketa = function()

        addText("My name is Aketa.", 2000)

    end,

    textDontKnow = function()

        addText("I don't know how I got here.", 2000)

    end,

    textScared = function()

        addText("But this place scares me.", 2000)

    end
}

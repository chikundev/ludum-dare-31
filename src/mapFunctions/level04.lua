return {

    moveLevel = function()

        currentBGM:stop()

        currentBGM = bgm.escape

        moveAllRelative('up1', 0, -2, 0)

        moveAllRelative('right1', 1, 0, 0)
        moveAllRelative('right1Spec', 1, 0, 0)
        moveAllRelative('right1Spec', 0, 1, 100)
        moveAllRelative('right2', 6, 0, 0)
        moveAllRelative('right2', 0, 2, 100)
        moveAllRelative('right2Spec', 2, 0, 0)
        moveAllRelative('right2Spec', 0, 2, 100)


        moveAllRelative('up2', 0, -1, 0)
        moveAllRelative('up3', 0, -3, 0)

        moveAllRelative('bridge1', -1, 0, 0)
        moveAllRelative('bridge1', 0, -8, 100)

        moveAllRelative('fixture1', 6, 0, 0)
        moveAllRelative('fixture1', 0, -14, 100)

        moveAllRelative('bridge2', 1, 0, 0)
        moveAllRelative('bridge2', 0, -6, 100)

        moveAllRelative('down6', 0, 6, 0)

        moveAllRelative('magic', 0, -8, 0)
    end,
    danger = function()

        addText("I feel like things are only going to get more difficult...", 4500)

    end,

    nextLevel = function()

        --Changes to level 5
        map.change(maps.level05, false)

        currentBGM:stop()

        currentBGM = bgm.world2

        addText("Christ almighty.", 2500)

    end

}

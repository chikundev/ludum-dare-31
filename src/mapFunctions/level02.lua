return {
    moveLevel = function()

        --Moves all tiles into place
        for a=1, 2 do
            moveAllRelative('topWall' .. a, 0, 1, 100)
        end

        for b=3, 4 do
            moveAllRelative('topWall' .. b, 0, 2, 100)
        end

        for c=5, 6 do
            moveAllRelative('topWall' .. c, 0, 3, 100)
        end

        for d=7, 8 do
            moveAllRelative('topWall' .. d, 0, 4, 100)
        end

        for e=9, 20 do
            moveAllRelative('topWall' .. e, 0, -4, 100)
        end

        for f=1, 3 do
            moveAllRelative('stair' .. f, 0, 3, 100)
        end

        for f=1, 3 do
            moveAllRelative('stair' .. (f + 2), 0, 2, 100)
        end

        moveTile('stair0', 25, 7, 100)

        for g=1, 9 do
            moveAllRelative('middleWall' .. g, 4, 0, 100)
        end

        for h=1, 4 do
            moveAllRelative('slidingFloor' .. h, 4, 0, 100)
        end

        for j=1, 4 do
            moveAllRelative('lavaFloor' .. j, 0, 1, 100)
        end

    end,

    makeStairs = function()

        --Creates a set of stairs
        moveTile('stairWell1', 12, 11, 0)
        moveTile('stairWell2', 11, 10, 0)
        moveTile('stairWell3', 10, 9, 0)
        moveTile('stairWell5', 8, 7, 0)
        moveTile('stairWell6', 7, 6, 0)
        moveTile('platform1', 2, 11, 0)
        moveTile('platform2', 6, 14, 0)
        moveTile('platform3', 2, 17, 0)

        for i=1, 6 do
            moveAllRelative('sideWall' .. i, -4, 0, 0)
        end

    end,

    coverLava = function()

        --Covers lava from level 1
        for k=1, 6 do
            moveAllRelative('coverLava' .. k, -2, 0, 0)
        end

        for l=1, 29 do
            moveAllRelative('moveFloor' .. l, 0, 6, 0)
        end
        for m=1, 4 do
            moveAllRelative('seal' .. m, -3, 0, 0)
        end

    end,

    textMaze = function()

        currentBGM:stop()

        currentBGM = bgm.complication

        addText("I wonder what it means.", 3000)

    end,

    textWayOut = function()

        addText("The maze is forever changing.", 3000)

    end,

    textUntilThen = function()

        addText("I hope that one day I'll find a way out.", 3000)

    end,

    textThis = function()

        currentBGM:stop()

        currentBGM = bgm.world1

        addText("What's this? A gate?", 3000)

    end,

    nextLevel = function()

        --Changes to level 3
        map.change(maps.level03, true)

        currentBGM:stop()

        currentBGM = bgm.world1

    end
}

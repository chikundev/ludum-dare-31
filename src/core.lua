-- chikun :: 2014
-- Core routines


core = { }      -- Create core table


function core.load()

    -- Load player functions
    require "src/player"

    -- Load play state
    state.load(states.splash)

end


function core.update(dt)

end


-- Draws underlay
function core.underlay()

end


-- Draws overlay
function core.overlay()



end
